package com.vgdragon.neuralnetwork;

import java.util.Arrays;

public class Network {

    public double[][] output;
    public double[][][] weights;
    public double[][] bias;


    public double[][] errorSignal;

    public double[][] outputDerivated;

    public final int[] NETWORK_LAYER_SIZEES;
    public final int INPUT_SIZE;
    public final int OUTPUT_SIZE;
    public final int NETWORK_SIZE;

    public Network(int... NETWORK_LAYER_SIZEES) {
        this.NETWORK_LAYER_SIZEES = NETWORK_LAYER_SIZEES;
        this.INPUT_SIZE = this.NETWORK_LAYER_SIZEES[0];
        this.NETWORK_SIZE = this.NETWORK_LAYER_SIZEES.length;
        this.OUTPUT_SIZE = this.NETWORK_LAYER_SIZEES[NETWORK_SIZE - 1];

        this.output = new double[NETWORK_SIZE][];
        this.weights = new double[NETWORK_SIZE][][];
        this.bias = new double[NETWORK_SIZE][];


        this.errorSignal = new double[NETWORK_SIZE][];
        this.outputDerivated = new double[NETWORK_SIZE][];

        for (int i = 0; i < NETWORK_SIZE; i++){
            this.output[i] = new double[NETWORK_LAYER_SIZEES[i]];

            this.errorSignal[i] = new double[NETWORK_LAYER_SIZEES[i]];
            this.outputDerivated[i] = new double[NETWORK_LAYER_SIZEES[i]];

            //this.bias[i] = new double[NETWORK_LAYER_SIZEES[i]];

            this.bias[i] = NetworkTools.createRandomArray(NETWORK_LAYER_SIZEES[i], 0.3,1);

            if (i > 0){
                //this.weights[i] = new double[NETWORK_LAYER_SIZEES[i]][NETWORK_LAYER_SIZEES[i - 1]];

                this.weights[i] = NetworkTools.createRandomArray(NETWORK_LAYER_SIZEES[i], NETWORK_LAYER_SIZEES[i - 1], -0.3, 0.9);
            }
        }
    }

    public double[] calculate(double... input){

        if(input.length != this.INPUT_SIZE) return null;

        this.output[0] = input;
        for(int layer = 1; layer < NETWORK_SIZE; layer++){
            for (int neuron = 0; neuron < NETWORK_LAYER_SIZEES[layer]; neuron++){
                double sum = bias[layer][neuron];

                for(int prevNeuron = 0; prevNeuron < NETWORK_LAYER_SIZEES[layer - 1]; prevNeuron++){
                    sum += output[layer - 1][prevNeuron] * weights[layer][neuron][prevNeuron];
                }

                output[layer][neuron] = sigmoid(sum);
                outputDerivated[layer][neuron] = (output[layer][neuron] * (1 - output[layer][neuron]));

            }
        }

        return output[NETWORK_SIZE - 1];

    }

    public void train(double[] input, double[] target, double eta){
        if(input.length != INPUT_SIZE || target.length != OUTPUT_SIZE) return;
        calculate(input);
        backpropError(input);
        updateWeights(eta);

    }

    public void backpropError(double[] target){
        for (int neuron = 0; neuron < NETWORK_LAYER_SIZEES[NETWORK_SIZE - 1]; neuron ++){
            errorSignal[NETWORK_SIZE - 1][neuron] = (output[NETWORK_SIZE - 1][neuron] - target[neuron]) * outputDerivated[NETWORK_SIZE - 1][neuron];

        }

        for (int layer = NETWORK_SIZE - 2; layer > 0; layer--){
            for (int neuron = 0; neuron < NETWORK_LAYER_SIZEES[layer]; neuron++){
                double sum = 0;
                for (int nextNeuron = 0; nextNeuron < NETWORK_LAYER_SIZEES[layer + 1]; nextNeuron++){
                    sum += weights[layer + 1][nextNeuron][neuron] * errorSignal[layer + 1][nextNeuron];
                }

                this.errorSignal[layer][neuron] = sum * outputDerivated[layer][neuron];

            }
        }
    }

    public void updateWeights(double eta){
        for(int layer = 1; layer < NETWORK_SIZE; layer++){
            for (int neuron = 0; neuron < NETWORK_LAYER_SIZEES[layer]; neuron++){
                for(int prevNeuron = 0; prevNeuron < NETWORK_LAYER_SIZEES[layer - 1]; prevNeuron++){
                    double delta = - eta * output[layer - 1][prevNeuron] * errorSignal[layer][neuron];

                    weights[layer][neuron][prevNeuron] += delta;
                }

                double  delta = - eta * errorSignal[layer][neuron];
                bias[layer][neuron] += delta;

            }


        }

    }

    private double sigmoid(double x){
        return 1d / (1 + Math.exp(-x));
    }


    public static void main(String[] args){
        Network net = new Network(4, 1, 3, 4);

        double[] input = new double[]{0.1, 0.5, 0.2, 0.9};
        double[] target = new double[]{0, 1, 0, 0};




        for(int i = 0; i < 10000000; i++){
            net.train(input, target, 1);
        }

        double[] output = net.calculate(input);

        System.out.println(Arrays.toString(output));

    }


}
